#!/usr/bin/env python

# Displays some usefule info about ldoms (logical domains) on a given
#  control domain(s). 
#
# Info includs:
#  -disk volumes used
#  -total vcpus, memory used
#  -where volumes are
#
# Certain assumptions are made:
#  -ldoms are using zvols as backend storage
#  -ssh public key auth is configured to the cdom(s) 
#  -sizes of memory configured in GB units
#
# TODO: list remaining vcpu and memory resources
 
import sys
import os

# Hardcoded default list of control domains.
cdoms=['put',
       'your',
       'domains',
       'here']

# If given on command line, replace with those on command line.
if len(sys.argv) > 1:
    if (sys.argv[1] == '-h' or sys.argv[1] == '-?'):
        print "%s [cdoms]\n\nList useful information about ldoms for given control domains\n" %(sys.argv[0])
        exit(0)
    cdoms=sys.argv[1:]

def get_output(server, command ):
    ssh_command='ssh -l root %s %s' %(server, command)
    f = os.popen(ssh_command)
    return map(str.strip,f.readlines())

# parse 'ldm -p' output
def get_field(line,key):
    fields=line.split('|')
    for f in fields:
        try:
            (k,val) = f.split('=')
            if k==key:
                return val
        except ValueError:
            pass
    return None
    
def get_zvol(path):
    zvol=os.path.basename(path)
    pool=os.path.basename(os.path.dirname(path))
    return os.path.join(pool,zvol)

def print_no_cr(stuff):
    sys.stdout.write(stuff)

for cdom in cdoms:
    print '%s' % (cdom )
    print '%s' % ('-' * len(cdom) )
    primary_disk_list=get_output(cdom, 'ldm list -o disk -p primary | grep vol=')
    pools = get_output(cdom,'zpool list -H -o name')
    disk_count=dict.fromkeys(pools,0)
    disk_free=dict.fromkeys(pools,0)
    
    # Get free space in zpool, eg df -h /rpool
    for p in pools:
        disk_free[p]=get_output(cdom, 'df -h /%s'%(p) )[1].split()[3] 

    # Sum up used ldom resources. There is another way to find free resources but this
    #  is simplest to understand. You have to know what the total available is.
    ldm_list=get_output(cdom, "ldm list | egrep 'active|bound' | grep -v inactive")
    vcpus=0
    memory=0
    for listing in ldm_list:
        vcpus+=int(listing.split()[4])
        try:
            memory+=int(listing.split()[5].strip('G'))  # Assuming all mem is integral Gigabytes
        except:
           print "ldom memory not integral amount of gigabytes for %s " %(listing.split()[0])

    # Get list of ldoms and the zvols used.
    for l in  get_output(cdom, 'ldm list -o domain -p | grep DOMAIN | grep -v primary'):
        domain_name=get_field(l,'name')
        disk_list=get_output(cdom, 'ldm list -o disk -p %s | grep VDISK' %(domain_name))
        print_no_cr ('%s: ' % (domain_name))
        for d in disk_list:
            (vol,x) = get_field(d,'vol').split('@')
            # grep
            for p in primary_disk_list:
                if vol==get_field(p,'vol'):
                    zvol=get_zvol(get_field(p,'dev'))
                    print_no_cr ('%s ' %(zvol))
                    pool=os.path.dirname(zvol)
                    disk_count[pool] +=1
        print

    print "\n  zpools usage:"
    for pool in disk_count.keys():
        print '       %s:\t%d zvols, %s free space' %(pool, disk_count[pool], disk_free[pool])

    print "\n  %d vcpus, %dG mem used by all ldoms on this server" %(vcpus,memory)
    print



